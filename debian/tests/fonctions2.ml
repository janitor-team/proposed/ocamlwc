(* fonctions dans un record *)
type 'a foo = {n:int; f: 'a -> 'a} ;;
let foo = {n=42; f=fun x -> x} ;;
foo.f 33;;

(* fonctions dans une paire.  *)
let fp = ( (fun x -> x+1), (fun x -> x*x) );;

(* attention aux parantheses *)
let fp = ( fun x -> x+1, fun x -> x*x );;

(* extraction des fonctions d'une paire *)
(fst fp) 42;;
(fst fp) ((snd fp) 10);;
(fst fp) (snd fp) 10;; (* attention aux parantheses *)
